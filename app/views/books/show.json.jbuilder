json.extract! @book, :id, :title, :author, :publisher, :published_date, :description, :isbns, :page_count, :print_type, :category, :language, :created_at, :updated_at
