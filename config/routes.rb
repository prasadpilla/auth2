Rails.application.routes.draw do
  get 'callbacks/facebook'
  get 'callbacks/google_oauth2'

  mount_devise_token_auth_for 'User', at: 'auth'
  scope '/api' do
    resources :books
  end 
end
